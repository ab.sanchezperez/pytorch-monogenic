# A robust Pytorch trainable entry ConVNet layer in Fourier domain

### Requirements

- [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html)

To install requirements:

```bash
conda env create -f environment.yml
```

Creates a conda environment called monogenic. You can access this with the following command:

```bash
conda activate monogenic
```


### Create the Monogenic layer

```python
from layers.monogenic import Monogenic

layer = Monogenic(
    sigma=0.1,       # Chose sigma value e.g. 0.1 
    wave_length=1.0  # Chose wave length value e.g. 1.0
)
```

### Add the layer to a Pytorch Module

```python
import torch.nn as nn
from layers.monogenic import Monogenic


class MyModule(nn.Module):
    def __init__(self):
        super(MyModule, self).__init__()
        self.mon = Monogenic()     # Always returns 6 channels
        self.conv = nn.Conv2d(6, 12, (3, 3))
        ...

    def forward(self, inputs):
        x = self.mon(inputs)
        x = self.conv(x)
        ...
        return x
```

### Layer feeding example 

```python
import torch
from layers.monogenic import Monogenic

input = torch.randn((1, 3, 128, 128))
# input = torch.randn((1, 1, 128, 128))  # Input with one channel
layer = Monogenic()
output = layer(input)                    # Output shape 1x6x128x128
```

### Layer output example

![](./images/output.png)

### Example

- CIFAR-10 classification using ConvNet and Monogenic layer with [Pytorch-Lightning](https://www.pytorchlightning.ai/).

To run the example do the following:

```bash
python test.py --gpus 1 --max_epochs 20
```

### License

Project is distributed under [MIT License](./LICENSE)
