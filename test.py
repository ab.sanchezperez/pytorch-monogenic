#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Guillermo Mendoza"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco, Universidad Autonoma de Guadalajara"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya", "Guillermo Mendoza"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import argparse

from modules.module import TestModule
from pytorch_lightning import loggers
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser = TestModule.add_model_specific_args(parent_parser=parser)
    parser = Trainer.add_argparse_args(parent_parser=parser)
    args = parser.parse_args()

    checkpoint = ModelCheckpoint(
        monitor='val_avg_acc', mode='max', verbose=True, filename='test-{epoch:03d}'
    )
    board_logger = loggers.TensorBoardLogger(save_dir=args.metrics_path)
    trainer = Trainer.from_argparse_args(args, callbacks=[checkpoint], logger=[board_logger])
    model = TestModule(args)
    trainer.fit(model=model)

    ckpt_path = checkpoint.best_model_path
    print('Best checkpoint path:', ckpt_path)
