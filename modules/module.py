#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Guillermo Mendoza"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco, Universidad Autonoma de Guadalajara"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya", "Guillermo Mendoza"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import argparse

from torch.optim import Adam
from torch.utils.data import DataLoader
from torch.nn.functional import cross_entropy
from pytorch_lightning.core import LightningModule
from pytorch_lightning.metrics.functional import accuracy
from layers.monogenic import Monogenic


class TestModule(LightningModule):
    
    def __init__(self, hparams):
        super(TestModule, self).__init__()
        self.hparams = hparams
        self.conv = nn.Sequential(
            Monogenic(),
            nn.Conv2d(6, 6, (5, 5)),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Conv2d(6, 16, (5, 5)),
            nn.ReLU(),
            nn.MaxPool2d(2, 2)
        )
        self.fc = nn.Sequential(
            nn.Linear(16 * 5 * 5, 120),
            nn.ReLU(),
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Linear(84, 10),
            nn.Softmax(1)
        )

    def forward(self, x):
        x = self.conv(x)
        x = x.view(-1, 16 * 5 * 5)
        x = self.fc(x)
        return x

    def configure_optimizers(self):
        optimizer = Adam(params=self.parameters(), lr=self.hparams.learning_rate)
        return optimizer

    def training_step(self, batch, batch_idx):
        images, targets = batch
        outputs = self(images)
        loss = cross_entropy(outputs, targets)
        acc = accuracy(outputs, targets)
        metrics = {
            'train_loss': loss,
            'train_acc': acc
        }
        self.log_dict(metrics, prog_bar=True)
        return {'loss': loss, 'train_loss': loss, 'train_acc': acc}

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([output['train_loss'] for output in outputs]).mean()
        avg_acc = torch.stack([output['train_acc'] for output in outputs]).mean()
        metrics = {
            'train_avg_loss': avg_loss,
            'train_avg_acc': avg_acc
        }
        self.log_dict(metrics)

    def validation_step(self, batch, batch_idx):
        images, targets = batch
        outputs = self(images)
        loss = cross_entropy(outputs, targets)
        acc = accuracy(outputs, targets)
        metrics = {
            'val_loss': loss,
            'val_acc': acc
        }
        self.log_dict(metrics)
        return {'loss': loss, 'val_loss': loss, 'val_acc': acc}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([output['val_loss'] for output in outputs]).mean()
        avg_acc = torch.stack([output['val_acc'] for output in outputs]).mean()
        metrics = {
            'val_avg_loss': avg_loss,
            'val_avg_acc': avg_acc
        }
        self.log_dict(metrics)

    def train_dataloader(self):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        dataset = torchvision.datasets.CIFAR10(root='./CIFAR10_data/', train=True, download=True, transform=transform)
        loader = DataLoader(dataset, batch_size=self.hparams.batch_size, shuffle=True, num_workers=4)
        return loader

    def val_dataloader(self):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        dataset = torchvision.datasets.CIFAR10(root='./CIFAR10_data/', train=False, download=True, transform=transform)
        loader = DataLoader(dataset, batch_size=self.hparams.batch_size, shuffle=False, num_workers=4)
        return loader

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = argparse.ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('-l', '--learning_rate', type=float, default=0.0001, required=False)
        parser.add_argument('-b', '--batch_size', type=int, default=32, required=False)
        parser.add_argument('-m', '--metrics_path', type=str, default='metrics', required=False)
        return parser
